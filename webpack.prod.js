const path = require("path")
const common = require("./webpack.common")
const { merge } = require("webpack-merge")
const { CleanWebpackPlugin } = require("clean-webpack-plugin")
const MiniCssExtractPlugin = require("mini-css-extract-plugin")
const OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin")
const TerserPlugin = require("terser-webpack-plugin")

module.exports = merge(common, {
	mode: "production",
	output: {
		filename: "assets/js/[name].[contentHash].bundlr.js",
		path: path.resolve(__dirname, "dist"),
	},
	optimization: {
		minimizer: [new OptimizeCssAssetsPlugin(), new TerserPlugin()],
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: "assets/css/[name].[contentHash].css",
		}),
		new CleanWebpackPlugin(),
	],
	module: {
		rules: [
			{
				test: /\.(sc|c)ss$/,
				use: [
					MiniCssExtractPlugin.loader, // 3. Extract CSS into files
					"css-loader", // 2. Turns css into commonjs
					"sass-loader", // 1. Turns SASS into CSS
				],
			},
			{
				test: /\.(gif|png|jpe?g)$/i,
				use: [
					{
						loader: "file-loader",
						options: {
							name: "[path][name].[ext]",
							context: path.resolve(__dirname, "src/"),
							useRelativePaths: true,
						},
					},
					{
						loader: "image-webpack-loader",
					},
				],
			},
			{
				test: /\.(eot|woff|woff2|ttf|svg)(\?\S*)?$/,
				loader: "file-loader",
				options: {
					limit: 10000,
					name: "[name].[ext]",
					outputPath: "assets/fonts", // where the fonts will go
					publicPath: "../fonts", // override the default path
				},
				exclude: [/\\assets\\fonts\\.*\.svg([\?\#].*)?$/],
			},
			{
				type: "javascript/auto",
				test: /\.json$/,
				use: [
					{
						loader: "file-loader",
						// include: [path.resolve(__dirname, "src")],
						options: {
							limit: 10000,
							name: "[name].[ext]",
							outputPath: "assets/js/data", // where the fonts will go
							// publicPath: "../js/data" // override the default path
						},
					},
				],
			},
		],
	},
})
