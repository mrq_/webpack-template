const path = require("path")
const Dotenv = require("dotenv-webpack")
const HtmlWebpackPlugin = require("html-webpack-plugin")
const HtmlWebpackPartialsPlugin = require("html-webpack-partials-plugin")

module.exports = {
	entry: {
		vendor: "./src/vendor.js",
		index: "./src/assets/js/index.js",
		contact: "./src/assets/js/contact.js",
		people: "./src/assets/js/people.js",
		gallery: "./src/assets/js/gallery.js"
	},
	plugins: [
		new HtmlWebpackPlugin({
			title: "_Webpack-Startr | Home",
			minify: {
				collapseWhitespace: false,
				removeComments: true
			},
			hash: true,
			filename: "index.html",
			excludeChunks: ["contact", "people", "gallery"],
			template: "./src/views/pages/index.ejs",
			meta: {
				description: "Free Web tutorials",
				keywords: "HTML,SCSS,JavaScript,Webpack",
				author: "Mohau R Letlala"
			}
		}),
		new HtmlWebpackPlugin({
			title: "_Webpack-Startr | Contact Us",
			minify: {
				collapseWhitespace: false,
				removeComments: true
			},
			hash: true,
			filename: "contact.html",
			excludeChunks: ["index", "gallery", "people"],
			template: "./src/views/pages/contact.ejs",
			meta: {
				description: "Free Web tutorials",
				keywords: "HTML,SCSS,JavaScript,Webpack",
				author: "Mohau R Letlala"
			}
		}),
		new HtmlWebpackPlugin({
			title: "_Webpack-Startr | People",
			minify: {
				collapseWhitespace: false,
				removeComments: true
			},
			hash: true,
			filename: "people.html",
			excludeChunks: ["index", "contact", "gallery"],
			template: "./src/views/pages/people.ejs",
			meta: {
				description: "Free Web tutorials",
				keywords: "HTML,SCSS,JavaScript,Webpack",
				author: "Mohau R Letlala"
			}
		}),
		new HtmlWebpackPlugin({
			title: "_Webpack-Startr | Gallery",
			minify: {
				collapseWhitespace: false,
				removeComments: true
			},
			hash: true,
			filename: "gallery.html",
			excludeChunks: ["index", "contact", "people"],
			template: "./src/views/pages/gallery.ejs",
			meta: {
				description: "Free Web tutorials",
				keywords: "HTML,SCSS,JavaScript,Webpack",
				author: "Mohau R Letlala"
			}
		}),
		new HtmlWebpackPartialsPlugin([
			{
				path: "./src/views/partials/nav.html",
				priority: "high",
				template_filename: "*"
			},
			{
				path: "./src/views/partials/footer.html",
				priority: "low",
				template_filename: "*"
			}
		]),
		new Dotenv()
	],
	module: {
		rules: [
			{
				test: /\.html$/,
				loader: "html-loader"
			}
		]
	}
}
