const path = require("path")
const { merge } = require("webpack-merge")
const common = require("./webpack.common")
const HtmlWebpackPlugin = require("html-webpack-plugin")

module.exports = merge(common, {
	mode: "development",
	output: {
		filename: "[name].bundlr.js",
		path: path.resolve(__dirname, "dist"),
	},
	devtool: "inline-source-map",
	plugins: [],
	module: {
		rules: [
			{
				test: /\.scss$/,
				use: [
					"style-loader", // 3. Injects styles into DOM
					"css-loader", // 2. Turns css into commonjs
					"sass-loader", // 1. Turns SASS into CSS
				],
			},
			{
				test: /\.(png|jp?g|gif)$/,
				use: {
					loader: "file-loader",
					options: {
						name: "[path][name].[ext]",
						context: path.resolve(__dirname, "src/"),
						useRelativePaths: true,
					},
				},
			},
			{
				test: /\.(eot|woff|woff2|ttf|svg)(\?\S*)?$/,
				loader: "file-loader",
				options: {
					limit: 10000,
					name: "[name].[ext]",
					outputPath: "assets/fonts", // where the fonts will go
				},
			},
			{
				type: "javascript/auto",
				test: /\.json$/,
				use: [
					{
						loader: "file-loader",
						options: {
							name: "[name].[ext]",
							outputPath: "assets/js/data", // where the fonts will go
						},
					},
				],
			},
		],
	},
})
