console.log("Vendor Bundle.")

/*********************************************
 *****  Import Scripts and Styling
 **/
import "./vendor.scss"
import $ from "jquery"
import "bootstrap"
window.jQuery = $
window.$ = $
/********************************************* */

/******************************************************
 * Document Ready
 ***/
$(function() {
	var current = window.location.href // 'href' property of the DOM element is the absolute path
	$("#navbarNav a").each(function() {
		// if the current path is like this link, make it active
		if (this.href === current) {
			$(this)
				.parent()
				.addClass("active") // Add class to parent of anchor tag
		}
	})

	// ===== Scroll to Top ====
	$(window).scroll(function() {
		if ($(this).scrollTop() >= 190) {
			// If page is scrolled more than 50px
			$("#return-to-top").fadeIn(200) // Fade in the arrow
		} else {
			$("#return-to-top").fadeOut(200) // Else fade out the arrow
		}
	})

	$("#return-to-top").click(function() {
		// When arrow is clicked
		$("body,html").animate(
			{
				scrollTop: 0 // Scroll to top of body
			},
			500
		)
	})
})
