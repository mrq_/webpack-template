const people = [
	{
		firstName: "Clark",
		lastName: "Kent",
		description:
			"Reporter: Stasea dicebantur. Hoc enim identidem dicitis, non intellegere nos quam dicatis voluptatem.",
		imgsrc: "/assets/img/p/clarkkent.png"
	},
	{
		firstName: "Bruce",
		lastName: "Wayne",
		description:
			"Playboy; \b Stasea dicebantur. Hoc enim identidem dicitis, non intellegere nos quam dicatis voluptatem.",
		imgsrc: "/assets/img/p/brucewayne.png"
	},
	{
		firstName: "Peter",
		lastName: "Parker",
		description:
			"Photographer; Stasea dicebantur. Hoc enim identidem dicitis, non intellegere nos quam dicatis voluptatem.",
		imgsrc: "/assets/img/p/peterparker.png"
	},
	{
		firstName: "Mary-Jane",
		lastName: "",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur. Hoc enim identidem dicitis, non intellegere nos quam dicatis voluptatem.",
		imgsrc: ""
	},
	{
		firstName: "Hawk Eye",
		lastName: "",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur. Hoc enim identidem dicitis, non intellegere nos quam dicatis voluptatem.",
		imgsrc: ""
	},
	{
		firstName: "Princess Diana of Themyscira",
		lastName: "",
		description:
			"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed haec ab Antiocho, familiari nostro, dicuntur multo melius et fortius, quam a Stasea dicebantur. Hoc enim identidem dicitis, non intellegere nos quam dicatis voluptatem.",
		imgsrc: "/assets/img/p/diana.png"
	}
]

export default people
