const contact = require("./../scss/contact.scss")

console.info("Contact...")

$(document).ready(function() {
	console.info("jQuery Ready!")
	console.table(process.env)
	/**
	 * Loading Scripts
	 */
	$.getScript("https://smtpjs.com/v3/smtp.js")
		.done(function(data, textStatus, jqxhr) {
			// console.log(textStatus) // Success
			// console.log(jqxhr.status) // 200
			console.log("Script is now Loaded and Executed.")
		})
		.fail(function(jqxhr, textStatus, exception) {
			// $("div.log").text("Triggered ajaxError handler.")
			// console.log(textStatus) // Error
			// console.log(jqxhr.status) // 404
			$(".alert").alert()
			$(".alert")
				.show()
				.addClass("alert-danger").prepend(`
						<h4 class="alert-heading">Unable to Send</h4>
						<p>Unable to send any messages currently.</p>
						<hr />
						<p class="mb-0">Please try again later.</p>
						`)
		})

	/**
	 * Form validation and email smtp.js
	 */
	// Fetch all the forms we want to apply custom Bootstrap validation styles to
	var forms = $(".needs-validation")

	// Loop over them and prevent submission
	var validation = Array.prototype.filter.call(forms, function(form) {
		$("form").submit(function(event) {
			if (!$(".honeypot").val()) {
				// console.log("Submitted")
				if (form.checkValidity() === false) {
					event.preventDefault()
					event.stopPropagation()
				}
				form.classList.add("was-validated")

				if (form.checkValidity()) {
					event.preventDefault()

					var data = {}
					data = {
						email: this.email.value,
						subject: this.selected.value,
						name: this.name.value,
						content: this.content.value
					}

					// console.table(data)
					// send the message and get a callback with an error or details of the message that was sent
					Email.send({
						SecureToken: process.env.EMAIL_TOKEN,
						Host: process.env.EMAIL_HOST,
						Username: process.env.EMAIL_USERNAME,
						Password: process.env.EMAIL_PASSWORD,
						To: process.env.EMAIL_USERNAME,
						From: data.email,
						Subject: data.subject,
						Body: `
						Name: <strong>${data.name}</strong> 
						<br/> 
						Email: <strong>${data.email}</strong>
						<br/> 
						Content: <br/> <p> ${data.content} </p>
						`
					}).then(function(response) {
						if (response === "OK") {
							event.preventDefault()
							$("#charBreak").hide()
							$(".alert")
								.show()
								.addClass("alert-success").prepend(`
									<h4 class="alert-heading">Thank You ${data.name}!</h4>
									<p>Your message has been sent.</p>
									`)

							$(".needs-validation").removeClass("was-validated")
							$("#contactForm").trigger("reset")
							window.setTimeout(function() {
								$(".alert").fadeOut("slow", function() {
									$(".alert").removeClass("alert-success")
									$(".alert").alert("close")
								})
							}, 5000)
						} else {
							$(".alert").alert()
							$(".alert")
								.show()
								.addClass("alert-danger").prepend(`
									<h4 class="alert-heading">Danger!</h4>
									<p>Unable to send at the moment.</p>
									<hr />
									<p class="mb-0">Please try again later.</p>
									`)
							window.setTimeout(function() {
								$(".alert").fadeOut("slow", function() {
									$(".alert").removeClass("alert-danger")
									$(".alert").alert("close")
								})
							}, 5000)
							event.preventDefault()
						}
					})
				}
			} else {
				$(".alert").alert()
				$(".alert")
					.show()
					.addClass("alert-warning").prepend(`
						<h4 class="alert-heading">Warning!</h4>
						<p>Unable to send at the moment.</p>
						<hr />
						<p class="mb-0">Please try again later.</p>
						`)
				window.setTimeout(function() {
					$(".alert").fadeOut("slow", function() {
						$(".alert").removeClass("alert-warning")
						$(".alert").alert("close")
					})
				}, 5000)
				event.preventDefault()
			}
		})
	})

	/*
	 **---------------------------------------------
	 ** Honeypot
	 **---------------------------------------------
	 */
	$(".honeypot").hide()

	/*
	 **---------------------------------------------
	 ** Count Characters in textarea
	 **---------------------------------------------
	 */
	$.fn.charCount = function($param) {
		var oldVal = "",
			currentVal = $param.val(),
			max = $param.attr("maxlength"),
			len = currentVal.length,
			char = max - len

		$("#charNum").show()
		$("#charBreak").show()
		if (currentVal == oldVal) {
			return //check to prevent multiple simultaneous triggers
		}

		if (len >= max) {
			$("#charNum").text(" you have reached the limit.")
			$("#charNum").addClass("text-danger")
			$("#charNum").removeClass("text-muted")
		} else {
			if (!$param.val) {
				char++
			}

			$("#charNum").text(" " + char + " characters.")
			$("#charNum").removeClass("text-danger")
			$("#charNum").addClass("text-muted")
		}

		return true
	}

	$("textarea")
		.on("mouseout", function(e) {
			if ($(this).val().length == 0) {
				$("#charNum").hide()
				$("#charBreak").hide()
			}
		})
		.on("change keyup paste", function(e) {
			$.fn.charCount($(this))
		})
})
