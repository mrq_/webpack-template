import "../scss/people.scss"
import { groupBy } from "lodash-es"
import people from "./data/people"

const profileImg = require.context("../img/p/", true, /\.(png|jpe?g)$/)

console.info("People...")
$(document).ready(function() {
	console.info("jQuery Ready!")

	let files = {},
		$target = $("#cardDeck"),
		$html = ``,
		count = 0,
		item_per_row = 3

	profileImg.keys().forEach(filename => {
		files[filename] = profileImg(filename)
	})

	$.each(people, function(index, data) {
		let fullName = data.firstName + " " + data.lastName
		let descrip = data.description
		let imgURL = data.imgsrc

		if (!imgURL || imgURL === null || imgURL === undefined) {
			imgURL = "/assets/img/p/default.png"
		}

		let backgroundImg = `style="background-image: url(${imgURL})"`

		/*optional stuff to do after success */
		if (count === 0) {
			// Start of a row
			$html += `
				<div class="row mb-3">
				`
		}

		$html += `
			<div class="col-sm-6 col-lg-4">
				<div class="card">
					<div class="card-img-top" ${backgroundImg} ></div>
					<div class="card-header"><h4 class="">${fullName}</h4></div>
					<div class="card-body">${descrip}</div>
				</div>
			</div>
			`
		++count
		if (count === item_per_row) {
			// End of row
			$html += `
				</div>
				`
			count = 0
		}

		//I only put this to show what these variables do for you
		// console.log("At index " + index + ", there is a value of " + fullName)
	})

	if (count > 0) {
		// Close the last row if it exist.
		$html += `
			</div>
			`
	}

	$target.html($html)
})
